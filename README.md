# HTML, CSS, JS

Welcome to the **HTML, CSS, JS** module!

During this module you will learn the basics of creating a webpage (HTML - HyperText Markup Language), styling it (Cascading Style Sheets) and adding dynamic behaviour (JavaScript).

Every part of the theory will be followed by the live coding session and exercises that should be done individually, with an assistance of the trainer.

Some of the topics that are going to be explained during this module are:
- HTML
- HTML5
- HTML forms
- CSS layout
- CSS pseudo-classes
- CSS3
- JavaScript
- ES6 JS
- TypeScript

## Presentation
Presentation is available under [this link](https://gitlab.com/sda-international/program/common/html-css-js/-/wikis/uploads/640b3a3660ee555ea8ed751db6cbd894/HTML_CSS_JS__1_.pdf).

 # Further reading

 ## Links
  1. [https://www.w3schools.com/html](https://www.w3schools.com/html)
  2. [https://www.w3schools.com/css](https://www.w3schools.com/css)
  3. [https://www.w3schools.com/js](https://www.w3schools.com/js)
  4. [https://www.typescriptlang.org/docs/home.html](https://www.typescriptlang.org/docs/home.html)
  5. [HTML Intro (2020) - Eli the Computer Guy](https://www.elithecomputerguy.com/html-intro-2020/)
  6. [Tutorial HTML, XHTML, HTML5 - **in Romanian**](https://tutorialehtml.com/ro/)
  7. [Learn HTML in 12 Minutes](https://www.youtube.com/watch?v=bWPMSSsVdPk)
  8. [17 Simple HTML Code Examples You Can Learn in 10 Minutes](https://www.makeuseof.com/tag/simple-html-code-learn-minutes/)
  9. [HTML tutorial on Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/HTML)
 10. [Beginner's roadmap to web development](https://www.freecodecamp.org/news/beginners-roadmap-web-development/)
